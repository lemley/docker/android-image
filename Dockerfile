FROM ubuntu:20.04

ENV ANDROID_SDK_ROOT "/sdk"
# Keep alias for compatibility
ENV ANDROID_HOME "${ANDROID_SDK_ROOT}"
ENV PATH "$PATH:${ANDROID_SDK_ROOT}/tools"
ENV DEBIAN_FRONTEND noninteractive
ENV GRADLE_USER_HOME "/root/.gradle"
ENV GRADLE_OPTS "-Dorg.gradle.daemon=true"

RUN apt-get -qq update \
 && apt install -qqy --no-install-recommends \
      bzip2 \
      openssh-client \
      curl \
      git-core \
      html2text \
      openjdk-11-jdk \
      libc6-i386 \
      lib32stdc++6 \
      lib32gcc1 \
      lib32z1 \
      unzip \
      locales \
      python3-minimal \
      python3-pip \
      wget \
      libreadline-dev \
      zlib1g-dev \
      rubygems \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# NPM & NODE update
RUN curl -s https://deb.nodesource.com/setup_16.x | bash
RUN apt-get install -y nodejs

RUN locale-gen en_US.UTF-8
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

RUN rm -f /etc/ssl/certs/java/cacerts; \
    /var/lib/dpkg/info/ca-certificates-java.postinst configure

ARG android_tools_version=6609375
RUN echo "Android command line tools version ${android_tools_version}"

RUN curl -s https://dl.google.com/android/repository/commandlinetools-linux-${android_tools_version}_latest.zip > /tools.zip \
 && mkdir -p ${ANDROID_SDK_ROOT}/cmdline-tools \
 && unzip /tools.zip -d ${ANDROID_SDK_ROOT}/cmdline-tools \
 && rm -v /tools.zip 

RUN mkdir -p $ANDROID_SDK_ROOT/licenses/ \
 && echo "8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e\n24333f8a63b6825ea9c5514f83c2829b004d1fee" > $ANDROID_SDK_ROOT/licenses/android-sdk-license \
 && echo "84831b9409646a918e30573bab4c9c91346d8abd\n504667f4c0de7af1a06de9f4b1727b84351f2910" > $ANDROID_SDK_ROOT/licenses/android-sdk-preview-license \
 && yes | ${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin/sdkmanager --sdk_root=${ANDROID_SDK_ROOT} --licenses >/dev/null \
 && touch /root/.android/repositories.cfg \
 && ${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin/sdkmanager --sdk_root=${ANDROID_SDK_ROOT} --update


ARG android_build_tools
ENV ANDROID_BUILD_TOOLS=$android_build_tools
ARG android_platform
ENV ANDROID_PLATFORM=$android_platform
RUN echo "buildtools: $ANDROID_BUILD_TOOLS"
RUN echo "platform: $ANDROID_PLATFORM"

ADD packages.txt /sdk
RUN while read -r package; do PACKAGES="${PACKAGES}${package} "; done < /sdk/packages.txt \
 && ${ANDROID_SDK_ROOT}/cmdline-tools/tools/bin/sdkmanager --sdk_root=${ANDROID_SDK_ROOT} ${PACKAGES}

## Install Gradle
ARG gradle_version=6.8.3
ENV PATH "$PATH:/opt/gradle/gradle-${gradle_version}/bin" 
RUN curl --location --show-error -O --url  https://services.gradle.org/distributions/gradle-${gradle_version}-bin.zip \
    && unzip -d /opt/gradle gradle-${gradle_version}-bin.zip \
    && ls /opt/gradle \
    && rm -v /gradle-${gradle_version}-bin.zip \
    && gradle -v  \
    && mkdir -p /tmp/foo

WorkDir /tmp/foo

RUN gradle init --type java-library --project-name foo \
   && ls -la \
   && gradle wrapper \
   && ./gradlew -v 

WorkDir /

RUN rm -rf /tmp/foo

# Install Google Cloud SDK
RUN curl -s https://dl.google.com/dl/cloudsdk/channels/rapid/google-cloud-sdk.tar.gz >  /tmp/google-cloud-sdk.tar.gz

RUN tar zxf /tmp/google-cloud-sdk.tar.gz --directory /opt \
    && /opt/google-cloud-sdk/install.sh --quiet

# Install rvm and correct ruby version
RUN curl -sSL https://get.rvm.io | bash -s stable
SHELL [ "/bin/bash", "-l", "-c" ]
RUN chmod +x /etc/profile.d/rvm.sh \
    && ./etc/profile.d/rvm.sh
RUN source /etc/profile.d/rvm.sh
RUN rvm install ruby-2.7.2-dev \
    && rvm use ruby-2.7.2-dev \
    && rvm list \
    && gem install date -v 3.1.1
